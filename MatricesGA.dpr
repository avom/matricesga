program MatricesGA;

uses
  Vcl.Forms,
  MatricesGA.MainForm in 'MatricesGA.MainForm.pas' {MainForm},
  MatricesGA.Individual in 'MatricesGA.Individual.pas',
  MatricesGA.Algorithm in 'MatricesGA.Algorithm.pas',
  MatricesGA.GASettings in 'MatricesGA.GASettings.pas',
  MatricesGA.OffspringGenerator in 'MatricesGA.OffspringGenerator.pas',
  MatricesGA.Matrix in 'MatricesGA.Matrix.pas',
  MatricesGA.PopulationOrder in 'MatricesGA.PopulationOrder.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Randomize;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
