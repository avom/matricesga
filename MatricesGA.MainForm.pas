unit MatricesGA.MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  MatricesGA.Individual, MatricesGA.Algorithm, Vcl.ComCtrls,
  MatricesGA.GASettings, MatricesGA.Matrix;

type
  TMainForm = class(TForm)
    FilesListBox: TListBox;
    MatricesGridPanel: TGridPanel;
    ButtonsPanel: TPanel;
    OriginalPaintBox: TPaintBox;
    BestPaintBox: TPaintBox;
    StartButton: TButton;
    NextButton: TButton;
    BestScoreEdit: TLabeledEdit;
    GenerationsEdit: TLabeledEdit;
    GenerationsUpDown: TUpDown;
    BestGenerationEdit: TLabeledEdit;
    CurrentGenerationEdit: TLabeledEdit;
    StopButton: TButton;
    SetupGroupBox: TGroupBox;
    CrossoverCheckBox: TCheckBox;
    PopulationSizeTrackBar: TTrackBar;
    PopulationSizeEdit: TLabeledEdit;
    BreedingPercentEdit: TLabeledEdit;
    BreedingPercentUpDown: TUpDown;
    MutationChanceUpDown: TUpDown;
    MutationChanceEdit: TLabeledEdit;
    ThreadCountEdit: TLabeledEdit;
    ThreadCountUpDown: TUpDown;
    AvgGenerationDurationEdit: TLabeledEdit;
    LastDurationEdit: TLabeledEdit;
    procedure FormCreate(Sender: TObject);
    procedure FilesListBoxClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure StopButtonClick(Sender: TObject);
    procedure PopulationSizeTrackBarChange(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    FAlgorithmStopped: Boolean;
    FMatrix: TByteMatrix;
    FAlgorithm: TGeneticAlgorithm;
    procedure ClearPaintBox(PaintBox: TPaintBox);
    procedure DrawMatrix(const Matrix: TByteMatrix; PaintBox: TPaintBox);
    procedure UpdateAlgorithmData;
    procedure RedrawPaintBoxes;
    function GetGASettings: TGASettings;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainForm: TMainForm;

implementation

uses
  System.Math;

{$R *.dfm}

procedure TMainForm.ClearPaintBox(PaintBox: TPaintBox);
begin
  PaintBox.Canvas.Brush.Style := bsSolid;
  PaintBox.Canvas.Brush.Color := clWhite;
  PaintBox.Canvas.Pen.Color := clWhite;
  PaintBox.Canvas.FillRect(PaintBox.ClientRect);
end;

constructor TMainForm.Create(AOwner: TComponent);
begin
  inherited;
  FAlgorithm := TGeneticAlgorithm.Create;
end;

destructor TMainForm.Destroy;
begin
  FAlgorithm.Free;
  inherited;
end;

procedure TMainForm.RedrawPaintBoxes;
begin
  if Assigned(FMatrix) then
    DrawMatrix(FMatrix, OriginalPaintBox)
  else
    ClearPaintBox(OriginalPaintBox);

  if NextButton.Enabled and Assigned(FAlgorithm.BestIndividual) then
    DrawMatrix(FAlgorithm.BestIndividual.ConstructMatrix, BestPaintBox)
  else
    ClearPaintBox(BestPaintBox);
end;

procedure TMainForm.DrawMatrix(const Matrix: TByteMatrix; PaintBox: TPaintBox);
const
  Padding = 5;
var
  Col, Row: Integer;
  ColCount, RowCount: Integer;
  Size: Integer;
  MinLeft, MinTop: Integer;
  Left, Top: Integer;
  Bitmap: TBitmap;
begin
  Bitmap := TBitmap.Create;
  try
    Bitmap.SetSize(PaintBox.ClientWidth, PaintBox.ClientHeight);
    ColCount := Length(Matrix);
    RowCount := Length(Matrix[0]);
    Size := Min((Bitmap.Height - 2 * Padding) div RowCount, (Bitmap.Width - 2 * Padding) div ColCount);

    MinLeft := (Bitmap.Width - ColCount * Size) div 2;
    MinTop := (Bitmap.Height - RowCount * Size) div 2;

    Bitmap.Canvas.Brush.Style := bsSolid;
    Bitmap.Canvas.Brush.Color := clWhite;
    Bitmap.Canvas.Pen.Color := clWhite;
    Bitmap.Canvas.FillRect(PaintBox.ClientRect);
    Bitmap.Canvas.Pen.Color := clBlack;

    for Col := 0 to ColCount - 1 do
    begin
      for Row := 0 to RowCount - 1 do
      begin
        Left := MinLeft + Col * Size;
        Top := MinTop + Row * Size;
        Bitmap.Canvas.Brush.Color := IfThen(Matrix[Col, Row] = 1, clBlack, clWhite);
        Bitmap.Canvas.Rectangle(Left, Top, Left + Size, Top + Size);
      end;
    end;
    PaintBox.Canvas.CopyRect(PaintBox.ClientRect, Bitmap.Canvas, PaintBox.ClientRect);
  finally
    Bitmap.Free;
  end;
end;

procedure TMainForm.FilesListBoxClick(Sender: TObject);
var
  Content: TStringList;
  FileName: string;
  Col, Row: Integer;
begin
  if FilesListBox.ItemIndex < 0 then
    Exit;

  Content := TStringList.Create;
  try
    FileName := FilesListBox.Items[FilesListBox.ItemIndex];
    Content.LoadFromFile(FileName);

    if Content.Count = 0 then
      Exit;

    SetLength(FMatrix, Length(Content[0]), Content.Count);
    for Row := 0 to Content.Count - 1 do
    begin
      for Col := 1 to Length(Content[Row]) do
        FMatrix[Col - 1, Row] := StrToInt(Content[Row][Col]);
    end;

    DrawMatrix(FMatrix, OriginalPaintBox);
    ClearPaintBox(BestPaintBox);

    StartButton.Enabled := True;
    NextButton.Enabled := False;
    SetupGroupBox.Enabled := True;
  finally
    Content.Free;
  end;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  FAlgorithmStopped := True;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  SearchRec: TSearchRec;
begin
  if FindFirst('*.matrix', faNormal, SearchRec) = 0 then
  begin
    try
      repeat
        FilesListBox.Items.Add(SearchRec.Name)
      until FindNext(SearchRec) <> 0;
    finally
      FindClose(SearchRec);
    end;
  end;
  PopulationSizeTrackBarChange(PopulationSizeTrackBar);
end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
  RedrawPaintBoxes;
end;

function TMainForm.GetGASettings: TGASettings;
begin
  Result.ThreadCount := StrToInt(ThreadCountEdit.Text);
  Result.PopulationSize := StrToInt(PopulationSizeEdit.Text);
  Result.BreedingPercent := StrToInt(BreedingPercentEdit.Text);
  Result.MutationChance := StrToInt(MutationChanceEdit.Text);
  Result.Crossover := CrossoverCheckBox.Checked;
end;

procedure TMainForm.NextButtonClick(Sender: TObject);
var
  i: Integer;
begin
  FAlgorithmStopped := False;
  for i := StrToInt(GenerationsEdit.Text) downto 1 do
  begin
    FAlgorithm.Step;
    UpdateAlgorithmData;
    Application.ProcessMessages;
    if FAlgorithmStopped then
      Break;
  end;
end;

procedure TMainForm.PopulationSizeTrackBarChange(Sender: TObject);
var
  i: Integer;
  PopSize: Integer;
begin
  PopSize := 1 + PopulationSizeTrackBar.Position mod 9;
  for i := 0 to PopulationSizeTrackBar.Position div 9 do
    PopSize := PopSize * 10;
  PopulationSizeEdit.Text := IntToStr(PopSize);
end;

procedure TMainForm.StartButtonClick(Sender: TObject);
begin
  StartButton.Enabled := False;
  NextButton.Enabled := True;
  SetupGroupBox.Enabled := False;
  FAlgorithm.Start(TMatrix.Create(FMatrix), GetGASettings);
  UpdateAlgorithmData;
end;

procedure TMainForm.StopButtonClick(Sender: TObject);
begin
  FAlgorithmStopped := True;
end;

procedure TMainForm.UpdateAlgorithmData;
var
  Matrix: TByteMatrix;
begin
  Matrix := FAlgorithm.BestIndividual.ConstructMatrix;
  if Assigned(Matrix) then
    DrawMatrix(Matrix, BestPaintBox);
  BestScoreEdit.Text := IntToStr(FAlgorithm.BestIndividual.Score);
  BestGenerationEdit.Text := IntToStr(FAlgorithm.BestIndividual.Generation);
  CurrentGenerationEdit.Text := IntToStr(FAlgorithm.Generation);
  AvgGenerationDurationEdit.Text := FormatDateTime('s.zzz', FAlgorithm.AverageDuration);
  LastDurationEdit.Text := FormatDateTime('s.zzz', FAlgorithm.LastDuration);
end;

end.
