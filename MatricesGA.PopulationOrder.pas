unit MatricesGA.PopulationOrder;

interface

uses
  System.Generics.Collections,
  MatricesGA.Individual,
  MatricesGA.Matrix;

type
  TPopulationOrder = class
  private
    FList: TObjectList<TIndividual>;
    FSameScoresBetweenLeftAndRight: Boolean;
    function NthsScore(N: Integer): TScore;
    function Partition(Left, Right, Pivot: Integer): Integer;
    function Select(Left, Right, N: Integer): TIndividual;
  public
    procedure MoveBreedingPopToTop(List: TObjectList<TIndividual>; BreedingPopSize: Integer);
  end;

implementation

uses
  Winapi.Windows, System.SysUtils;

{ TPopulationOrder }

procedure TPopulationOrder.MoveBreedingPopToTop(List: TObjectList<TIndividual>;
  BreedingPopSize: Integer);
var
  CriticalScore: TScore;
  i: Integer;
  Count: Integer;
  Time1, Time2, Time3: Cardinal;
begin
  Assert(Assigned(List));
  FList := List;
  Time1 := GetTickCount;
  CriticalScore := NthsScore(BreedingPopSize);
  Time2 := GetTickCount;

  Count := 0;
  for i := 0 to FList.Count - 1 do
  begin
    if FList[i].Score >= CriticalScore then
    begin
      FList.Exchange(i, Count);
      Inc(Count);
      if BreedingPopSize <= Count then
        Break;
    end;
  end;
  Time3 := GetTickCount;
  OutputDebugString(PChar('MoveBreedingPopToTop: ' + IntToStr(Time2 - Time1)));
  OutputDebugString(PChar('MoveBreedingPopToTop: ' + IntToStr(Time3 - Time2)));
end;

function TPopulationOrder.NthsScore(N: Integer): TScore;
begin
  Result := Select(0, FList.Count - 1, N - 1).Score;
end;

function TPopulationOrder.Partition(Left, Right, Pivot: Integer): Integer;
var
  i: Integer;
  PivotScore: TScore;
begin
  PivotScore := FList[Pivot].Score;
  FList.Exchange(Pivot, Right);
  Result := Left;
  for i := Left to Right - 1 do
  begin
    if FList[i].Score > PivotScore then
    begin
      FList.Exchange(Result, i);
      Inc(Result);
    end;
    FSameScoresBetweenLeftAndRight :=
      FSameScoresBetweenLeftAndRight and (FList[i].Score = PivotScore);
  end;
  FList.Exchange(Right, Result);
end;

function TPopulationOrder.Select(Left, Right, N: Integer): TIndividual;
var
  Pivot: Integer;
//  Time0: Cardinal;
begin
  if Left = Right then
    Exit(FList[Left]);

  repeat
    FSameScoresBetweenLeftAndRight := True;
//    Time0 := GetTickCount;
    Pivot := Left + Random(Right - Left + 1);
    Pivot := Partition(Left, Right, Pivot);
    if N < Pivot then
      Right := Pivot - 1
    else if N > Pivot then
      Left := Pivot + 1;
//    OutputDebugString(PChar(Format('Select: %d %d - %d', [Left, Right, GetTickCount - Time0])));
  until (N = Pivot) or FSameScoresBetweenLeftAndRight;
  Result := FList[N];
end;

end.
