unit MatricesGA.OffspringGenerator;

interface

uses
  System.Classes,
  System.Generics.Collections,
  MatricesGA.GASettings,
  MatricesGA.Individual;

type
  TOffspringGenerator = class(TThread)
  private
    FNewPopulation: TList<TIndividual>;
    FSettings: TGASettings;
    FOldPopulation: TList<TIndividual>;
    FGeneratorId: Integer;
  protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;

    procedure AddPreCreatedIndividuals(List: TList<TIndividual>; Index, Count: Integer);

    property GeneratorId: Integer read FGeneratorId write FGeneratorId;
    property NewPopulation: TList<TIndividual> read FNewPopulation;
    property OldPopulation: TList<TIndividual> read FOldPopulation write FOldPopulation;
    property Settings: TGASettings read FSettings write FSettings;
  end;

implementation

uses
  System.Generics.Defaults,
  System.SysUtils,
  Winapi.Windows;

{ TOffspringGenerator }

procedure TOffspringGenerator.AddPreCreatedIndividuals(List: TList<TIndividual>;
  Index, Count: Integer);
var
  i: Integer;
begin
  FNewPopulation.Capacity := Count;
  for i := Index to Index + Count - 1 do
    FNewPopulation.Add(List[i]);
end;

constructor TOffspringGenerator.Create;
begin
  inherited Create(True);
  FNewPopulation := TList<TIndividual>.Create;
end;

destructor TOffspringGenerator.Destroy;
begin
  FNewPopulation.Free;
  inherited;
end;

procedure TOffspringGenerator.Execute;
var
  MutationChance: Double;
  BreedingSelection: Integer;
  Offspring: TIndividual;
  Parent, Parent1, Parent2: TIndividual;
begin
  MutationChance := FSettings.MutationChance / 100;
  BreedingSelection := FSettings.BreedingPopSize;

  for Offspring in FNewPopulation do
  begin
    if FSettings.Crossover then
    begin
      repeat
        Parent1 := FOldPopulation[Random(BreedingSelection)];
        Parent2 := FOldPopulation[Random(BreedingSelection)];
      until Parent1 <> Parent2;

      Offspring.InheritGenes(Parent1, Parent2, MutationChance);
    end
    else
    begin
      Parent := FOldPopulation[Random(BreedingSelection)];
      Offspring.InheritGenes(Parent, MutationChance);
    end;
  end;
end;

end.
