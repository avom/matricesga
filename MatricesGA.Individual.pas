unit MatricesGA.Individual;

interface

uses
  MatricesGA.Matrix;

type
  TIndividual = class
  private
    FMatrix: TMatrix;
    FScore: Integer;
    FGeneration: Integer;
    FColGenes: TArray<TGene>;
    FRowGenes: TArray<TGene>;
    FUsedGenes: TArray<Boolean>;
    procedure Crossover(const ChildGenes, Parent1Genes, Parent2Genes: TArray<TGene>);
    procedure FisherYatesShuffle(const Genes: TArray<TGene>);
    function GetScore: Integer;
    procedure Mutate;
    procedure SwapGenes(const Genes: TArray<TGene>; const Gene1, Gene2: Integer);
  public
    constructor Create(const Matrix: TMatrix);
    constructor CreateChild(const Parent: TIndividual; const MutationChance: Double); overload;
    constructor CreateChild(const Parent1, Parent2: TIndividual; const MutationChance: Double); overload;
    constructor CreateCopy(const Individual: TIndividual);
    constructor CreateRandom(const Matrix: TMatrix);

    function ConstructMatrix: TByteMatrix;
    procedure InheritGenes(const Parent: TIndividual; const MutationChance: Double); overload;
    procedure InheritGenes(const Parent1, Parent2: TIndividual; const MutationChance: Double); overload;

    property Generation: Integer read FGeneration;
    property Score: Integer read FScore;
  end;

implementation

uses
  System.Math;

{ TIndividual }

function TIndividual.ConstructMatrix: TByteMatrix;
var
  Col, Row: Integer;
begin
  SetLength(Result, FMatrix.ColCount, FMatrix.RowCount);
  for Col := 0 to FMatrix.ColCount - 1 do
  begin
    for Row := 0 to FMatrix.RowCount - 1 do
      Result[Col, Row] := FMatrix[FColGenes[Col], FRowGenes[Row]];
  end;
end;

constructor TIndividual.Create(const Matrix: TMatrix);
var
  Col, Row: Integer;
begin
  SetLength(FColGenes, Matrix.ColCount);
  SetLength(FRowGenes, Matrix.RowCount);

  for Col := 0 to Matrix.ColCount - 1 do
    FColGenes[Col] := Col;
  for Row := 0 to Matrix.RowCount - 1 do
    FRowGenes[Row] := Row;

  FMatrix := Matrix;
  SetLength(FUsedGenes, Max(FMatrix.ColCount, FMatrix.RowCount));
  FScore := GetScore;
end;

constructor TIndividual.CreateChild(const Parent: TIndividual; const MutationChance: Double);
begin
  FMatrix := Parent.FMatrix;
  Setlength(FColGenes, Length(Parent.FColGenes));
  Setlength(FRowGenes, Length(Parent.FRowGenes));
  SetLength(FUsedGenes, Max(FMatrix.ColCount, FMatrix.RowCount));
  InheritGenes(Parent, MutationChance);
end;

constructor TIndividual.CreateChild(const Parent1, Parent2: TIndividual; const MutationChance: Double);
begin
  FMatrix := Parent1.FMatrix;
  Setlength(FColGenes, Length(Parent1.FColGenes));
  Setlength(FRowGenes, Length(Parent1.FRowGenes));
  SetLength(FUsedGenes, Max(FMatrix.ColCount, FMatrix.RowCount));
  InheritGenes(Parent1, Parent2, MutationChance);
end;

constructor TIndividual.CreateCopy(const Individual: TIndividual);
begin
  FMatrix := Individual.FMatrix;
  FColGenes := Copy(Individual.FColGenes, 0, MaxInt);
  FRowGenes := Copy(Individual.FRowGenes, 0, MaxInt);
  SetLength(FUsedGenes, Max(FMatrix.ColCount, FMatrix.RowCount));
  FScore := Individual.Score;
  FGeneration := Individual.FGeneration;
end;

constructor TIndividual.CreateRandom(const Matrix: TMatrix);
begin
  Create(Matrix);
  FisherYatesShuffle(FColGenes);
  FisherYatesShuffle(FRowGenes);
  FScore := GetScore;
end;

procedure TIndividual.Crossover(const ChildGenes, Parent1Genes, Parent2Genes: TArray<TGene>);
var
  Left, Right, Temp: Integer;
  i: Integer;
  k: Integer;
  Gene: Integer;
begin
  repeat
    Left := Random(High(ChildGenes));
    Right := Random(High(ChildGenes));
  until Left <= Right;

  FillChar(FUsedGenes[0], Length(FUsedGenes), 0);
  for i := Left to Right do
  begin
    Gene := Parent1Genes[i];
    ChildGenes[i] := Gene;
    FUsedGenes[Gene] := True;
  end;

  k := IfThen(Left = 0, Right + 1);
  for i := 0 to High(ChildGenes) do
  begin
    Gene := Parent2Genes[i];
    if not FUsedGenes[Gene] then
    begin
      FUsedGenes[Gene] := True;
      ChildGenes[k] := Gene;
      Inc(k);
      if k = Left then
        k := Right + 1;
    end;
  end;
end;

procedure TIndividual.FisherYatesShuffle(const Genes: TArray<TGene>);
var
  N, K: Integer;
begin
  N := Length(Genes);
  while N > 1 do
  begin
    Dec(N);
    K := Random(N + 1);
    SwapGenes(Genes, N, K);
  end;
end;

function TIndividual.GetScore: Integer;
var
  Col, Row: Integer;
begin
  Result := 0;
  for Col := 1 to FMatrix.ColCount - 1 do
    Inc(Result, FMatrix.GetColPairScore(FColGenes[Col - 1], FColGenes[Col]));
  for Row := 1 to FMatrix.RowCount - 1 do
    Inc(Result, FMatrix.GetRowPairScore(FRowGenes[Row - 1], FRowGenes[Row]));
end;

procedure TIndividual.InheritGenes(const Parent: TIndividual; const MutationChance: Double);
begin
  FScore := -1;
  if Random < MutationChance then
    Mutate;
  FGeneration := Parent.Generation + 1;
  FScore := GetScore;
end;

procedure TIndividual.InheritGenes(const Parent1, Parent2: TIndividual; const MutationChance: Double);
begin
  Crossover(FColGenes, Parent1.FColGenes, Parent2.FColGenes);
  Crossover(FRowGenes, Parent1.FRowGenes, Parent2.FRowGenes);
  if Random < MutationChance then
    Mutate;
  FGeneration := Parent1.Generation + 1;
  FScore := GetScore;
end;

procedure TIndividual.Mutate;
begin
  SwapGenes(FColGenes, Random(FMatrix.ColCount), Random(FMatrix.ColCount));
  SwapGenes(FRowGenes, Random(FMatrix.RowCount), Random(FMatrix.RowCount));
end;

procedure TIndividual.SwapGenes(const Genes: TArray<TGene>; const Gene1, Gene2: Integer);
var
  Temp: Integer;
begin
  Temp := Genes[Gene1];
  Genes[Gene1] := Genes[Gene2];
  Genes[Gene2] := Temp;
end;

end.
