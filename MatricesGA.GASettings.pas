unit MatricesGA.GASettings;

interface

type
  TGASettings = record
  private
    FPopulationSize: Integer;
    FBreedingPercent: Integer;
    FCrossover: Boolean;
    FMutationChance: Integer;
    FThreadCount: Integer;
    procedure SetBreedingPercent(const Value: Integer);
    procedure SetMutationChance(const Value: Integer);
    procedure SetPopulationSize(const Value: Integer);
    procedure SetThreadCount(const Value: Integer);
    function GetBreedingPopSize: Integer;
  public
    property ThreadCount: Integer read FThreadCount write SetThreadCount;
    property PopulationSize: Integer read FPopulationSize write SetPopulationSize;
    property BreedingPercent: Integer read FBreedingPercent write SetBreedingPercent;
    property MutationChance: Integer read FMutationChance write SetMutationChance;
    property Crossover: Boolean read FCrossover write FCrossover;

    property BreedingPopSize: Integer read GetBreedingPopSize;
  end;

implementation

uses
  System.Math;

{ TGASettings }

function TGASettings.GetBreedingPopSize: Integer;
begin
  Result := Max(1, BreedingPercent * PopulationSize div 100);
end;

procedure TGASettings.SetBreedingPercent(const Value: Integer);
begin
  FBreedingPercent := Max(1, Min(99, Value));
end;

procedure TGASettings.SetMutationChance(const Value: Integer);
begin
  FMutationChance := Max(0, Min(100, Value));
end;

procedure TGASettings.SetPopulationSize(const Value: Integer);
begin
  FPopulationSize := Max(10, Value);
end;

procedure TGASettings.SetThreadCount(const Value: Integer);
begin
  FThreadCount := Max(1, Value);
end;

end.
