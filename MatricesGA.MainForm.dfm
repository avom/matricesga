object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Matrices GA'
  ClientHeight = 778
  ClientWidth = 839
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object FilesListBox: TListBox
    Left = 0
    Top = 0
    Width = 181
    Height = 778
    Align = alLeft
    ItemHeight = 13
    TabOrder = 0
    OnClick = FilesListBoxClick
  end
  object MatricesGridPanel: TGridPanel
    Left = 181
    Top = 0
    Width = 449
    Height = 778
    Align = alClient
    Caption = 'grpnlMatrices'
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = OriginalPaintBox
        Row = 0
      end
      item
        Column = 0
        Control = BestPaintBox
        Row = 1
      end>
    RowCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end
      item
        SizeStyle = ssAuto
      end>
    ShowCaption = False
    TabOrder = 1
    object OriginalPaintBox: TPaintBox
      Left = 1
      Top = 1
      Width = 447
      Height = 388
      Align = alClient
      ExplicitLeft = 4
      ExplicitTop = 3
      ExplicitWidth = 535
      ExplicitHeight = 382
    end
    object BestPaintBox: TPaintBox
      Left = 1
      Top = 389
      Width = 447
      Height = 388
      Align = alClient
      ExplicitLeft = -1
      ExplicitTop = 390
    end
  end
  object ButtonsPanel: TPanel
    Left = 630
    Top = 0
    Width = 209
    Height = 778
    Align = alRight
    Caption = 'ButtonsPanel'
    ShowCaption = False
    TabOrder = 2
    object StartButton: TButton
      Left = 9
      Top = 183
      Width = 62
      Height = 25
      Caption = 'Start'
      Enabled = False
      TabOrder = 0
      OnClick = StartButtonClick
    end
    object NextButton: TButton
      Left = 74
      Top = 183
      Width = 62
      Height = 25
      Caption = 'Next'
      Enabled = False
      TabOrder = 1
      OnClick = NextButtonClick
    end
    object BestScoreEdit: TLabeledEdit
      Left = 102
      Top = 238
      Width = 99
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 50
      EditLabel.Height = 13
      EditLabel.Caption = 'Best score'
      LabelPosition = lpLeft
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 2
    end
    object GenerationsEdit: TLabeledEdit
      Left = 102
      Top = 214
      Width = 83
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 83
      EditLabel.Height = 13
      EditLabel.Caption = 'Next generations'
      LabelPosition = lpLeft
      NumbersOnly = True
      TabOrder = 3
      Text = '1'
    end
    object GenerationsUpDown: TUpDown
      Left = 185
      Top = 214
      Width = 16
      Height = 21
      Associate = GenerationsEdit
      Min = 1
      Max = 10000
      Position = 1
      TabOrder = 4
    end
    object BestGenerationEdit: TLabeledEdit
      Left = 102
      Top = 262
      Width = 99
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 76
      EditLabel.Height = 13
      EditLabel.Caption = 'Best generation'
      LabelPosition = lpLeft
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 5
    end
    object CurrentGenerationEdit: TLabeledEdit
      Left = 102
      Top = 286
      Width = 99
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 92
      EditLabel.Height = 13
      EditLabel.Caption = 'Current generation'
      LabelPosition = lpLeft
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 6
    end
    object StopButton: TButton
      Left = 139
      Top = 183
      Width = 62
      Height = 25
      Caption = 'Stop'
      TabOrder = 7
      OnClick = StopButtonClick
    end
    object SetupGroupBox: TGroupBox
      Left = 6
      Top = 5
      Width = 195
      Height = 172
      Caption = 'GA Settings'
      TabOrder = 8
      object CrossoverCheckBox: TCheckBox
        Left = 35
        Top = 149
        Width = 68
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Crossover'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object PopulationSizeTrackBar: TTrackBar
        Left = 1
        Top = 68
        Width = 193
        Height = 28
        Max = 45
        Position = 18
        TabOrder = 1
        OnChange = PopulationSizeTrackBarChange
      end
      object PopulationSizeEdit: TLabeledEdit
        Left = 90
        Top = 41
        Width = 99
        Height = 21
        Alignment = taRightJustify
        EditLabel.Width = 71
        EditLabel.Height = 13
        EditLabel.Caption = 'Population size'
        LabelPosition = lpLeft
        NumbersOnly = True
        ReadOnly = True
        TabOrder = 2
      end
      object BreedingPercentEdit: TLabeledEdit
        Left = 90
        Top = 102
        Width = 83
        Height = 21
        Alignment = taRightJustify
        EditLabel.Width = 56
        EditLabel.Height = 13
        EditLabel.Caption = 'Breeding %'
        LabelPosition = lpLeft
        NumbersOnly = True
        TabOrder = 3
        Text = '20'
      end
      object BreedingPercentUpDown: TUpDown
        Left = 173
        Top = 102
        Width = 16
        Height = 21
        Associate = BreedingPercentEdit
        Min = 1
        Max = 99
        Position = 20
        TabOrder = 4
      end
      object MutationChanceUpDown: TUpDown
        Left = 173
        Top = 126
        Width = 16
        Height = 21
        Position = 100
        TabOrder = 5
      end
      object MutationChanceEdit: TLabeledEdit
        Left = 90
        Top = 126
        Width = 83
        Height = 21
        Alignment = taRightJustify
        EditLabel.Width = 56
        EditLabel.Height = 13
        EditLabel.Caption = 'Mutation %'
        LabelPosition = lpLeft
        NumbersOnly = True
        TabOrder = 6
        Text = '100'
      end
      object ThreadCountEdit: TLabeledEdit
        Left = 90
        Top = 17
        Width = 83
        Height = 21
        Alignment = taRightJustify
        EditLabel.Width = 39
        EditLabel.Height = 13
        EditLabel.Caption = 'Threads'
        LabelPosition = lpLeft
        NumbersOnly = True
        ReadOnly = True
        TabOrder = 7
        Text = '1'
      end
      object ThreadCountUpDown: TUpDown
        Left = 173
        Top = 17
        Width = 16
        Height = 21
        Associate = ThreadCountEdit
        Min = 1
        Position = 1
        TabOrder = 8
      end
    end
    object AvgGenerationDurationEdit: TLabeledEdit
      Left = 102
      Top = 310
      Width = 99
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 84
      EditLabel.Height = 13
      EditLabel.Caption = 'Average duration'
      LabelPosition = lpLeft
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 9
    end
    object LastDurationEdit: TLabeledEdit
      Left = 102
      Top = 334
      Width = 99
      Height = 21
      Alignment = taRightJustify
      EditLabel.Width = 63
      EditLabel.Height = 13
      EditLabel.Caption = 'Last duration'
      LabelPosition = lpLeft
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 10
    end
  end
end
