unit MatricesGA.Algorithm;

interface

uses
  System.Generics.Collections,
  System.SysUtils,
  Winapi.Windows,
  MatricesGA.GASettings,
  MatricesGA.Individual,
  MatricesGA.OffspringGenerator,
  MatricesGA.Matrix,
  MatricesGA.PopulationOrder;

type
  TGeneticAlgorithm = class
  private
    FMatrix: TMatrix;
    FBestIndividual: TIndividual;
    FPopulation: TObjectList<TIndividual>;
    FOldPopulation: TObjectList<TIndividual>;
    FGeneration: Integer;
    FSettings: TGASettings;
    FDuration: TDateTime;
    FLastDuration: TDateTime;
    FPopulationOrder: TPopulationOrder;
    procedure GenerateOldPopulation;
    procedure GenerateRandomPopulation;
    function GetAverageDuration: TDateTime;
    procedure NextGeneration;
    procedure UpdateBestIndividual;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Start(const Matrix: TMatrix; const Settings: TGASettings);
    procedure Step(const Generations: Integer = 1);

    property AverageDuration: TDateTime read GetAverageDuration;
    property BestIndividual: TIndividual read FBestIndividual;
    property Generation: Integer read FGeneration;
    property LastDuration: TDateTime read FLastDuration;
  end;

implementation

uses
  System.Generics.Defaults,
  System.Classes;

{ TGeneticAlgorithm }

constructor TGeneticAlgorithm.Create;
begin
  FPopulation := TObjectList<TIndividual>.Create;
  FOldPopulation := TObjectList<TIndividual>.Create;
  FPopulationOrder := TPopulationOrder.Create;
end;

destructor TGeneticAlgorithm.Destroy;
begin
  FPopulation.Free;
  FOldPopulation.Free;
  FBestIndividual.Free;
  FPopulationOrder.Free;
  inherited;
end;

procedure TGeneticAlgorithm.GenerateOldPopulation;
var
  i: Integer;
begin
  FOldPopulation.Clear;
  for i := 0 to FSettings.PopulationSize - 1 do
    FOldPopulation.Add(TIndividual.Create(FMatrix));
end;

procedure TGeneticAlgorithm.GenerateRandomPopulation;
var
  i: Integer;
begin
  FPopulation.Clear;
  for i := 0 to FSettings.PopulationSize - 1 do
    FPopulation.Add(TIndividual.CreateRandom(FMatrix));
  FPopulationOrder.MoveBreedingPopToTop(FPopulation, FSettings.BreedingPopSize);
end;

function TGeneticAlgorithm.GetAverageDuration: TDateTime;
begin
  if FGeneration > 0 then
    Result := FDuration / FGeneration
  else
    Result := 0;
end;

procedure TGeneticAlgorithm.Start(const Matrix: TMatrix; const Settings: TGASettings);
begin
  FMatrix := Matrix;
  FSettings := Settings;
  FBestIndividual.Free;
  FBestIndividual := TIndividual.Create(Matrix);

  GenerateRandomPopulation;
  GenerateOldPopulation;
  UpdateBestIndividual;
  FGeneration := 0;
  FDuration := 0;
  FLastDuration := 0;
end;

procedure TGeneticAlgorithm.Step(const Generations: Integer = 1);
var
  i: Integer;
begin
  Assert(Generations > 0);
  for i := 1 to Generations do
    NextGeneration;
end;

procedure TGeneticAlgorithm.UpdateBestIndividual;
var
  i: Integer;
  Best: TIndividual;
begin
  Best := FBestIndividual;
  for i := 0 to FSettings.BreedingPopSize - 1 do
  begin
    if FPopulation[i].Score > Best.Score then
      Best := FPopulation[i];
  end;

  if Best <> FBestIndividual then
  begin
    FBestIndividual.Free;
    FBestIndividual := TIndividual.CreateCopy(Best);
  end;
end;

procedure TGeneticAlgorithm.NextGeneration;
var
  i: Integer;
  NewPopulation: TObjectList<TIndividual>;
  Generator: TOffspringGenerator;
  Generators: TObjectList<TOffspringGenerator>;
  PopPerThread: Integer;
  Time1, Time2, Time3, Time4, Time5, Time6: Cardinal;
  StartTime: TDateTime;
begin
  Time1 := GetTickCount;
  StartTime := Now;
  NewPopulation := FOldPopulation;
  Generators := TObjectList<TOffspringGenerator>.Create;
  try
    PopPerThread := NewPopulation.Count div FSettings.ThreadCount;
    for i := 0 to FSettings.ThreadCount - 1 do
    begin
      Generator := TOffspringGenerator.Create;
      Generators.Add(Generator);
      Generator.GeneratorId := i + 1;
      Generator.OldPopulation := FPopulation;
      Generator.AddPreCreatedIndividuals(NewPopulation, i * PopPerThread, PopPerThread);
      Generator.Settings := FSettings;
      Generator.Start;
    end;
    Time2 := GetTickCount;

    for Generator in Generators do
    begin
      while not Generator.Finished do
        TThread.Sleep(10);
    end;
    Time3 := GetTickCount;

    FPopulationOrder.MoveBreedingPopToTop(NewPopulation, FSettings.BreedingPopSize);
    Time4 := GetTickCount;

    UpdateBestIndividual;
    Time5 := GetTickCount;
  finally
    Generators.Free;
    FOldPopulation := FPopulation;
    FPopulation := NewPopulation;
    Inc(FGeneration);
  end;
  FLastDuration := Now - StartTime;
  FDuration := FDuration + FLastDuration;
  Time6 := GetTickCount;

  OutputDebugString(PChar(IntToStr(Time2 - Time1)));
  OutputDebugString(PChar(IntToStr(Time3 - Time2)));
  OutputDebugString(PChar(IntToStr(Time4 - Time3)));
  OutputDebugString(PChar(IntToStr(Time5 - Time4)));
  OutputDebugString(PChar(IntToStr(Time6 - Time5)));
end;

end.
