unit MatricesGA.Matrix;

interface

type
  // Cannot do generic matrix in XE5
  TGene = SmallInt;
  TByteMatrix = array of array of TGene;
  TIntMatrix = array of array of Integer;
  TScore = Integer;
  TScoreMatrix = TIntMatrix;

  TMatrix = record
  private
    FMatrix: TByteMatrix;
    FColCount: Integer;
    FRowCount: Integer;
    FColPairScores: TScoreMatrix;
    FRowPairScores: TScoreMatrix;
    procedure CalcColPairScores;
    procedure CalcRowPairScores;
    function GetMatrix(const Col, Row: Integer): Byte;
  public
    constructor Create(const Matrix: TByteMatrix);

    function GetColPairScore(const Col1, Col2: Integer): TScore;
    function GetRowPairScore(const Row1, Row2: Integer): TScore;

    property ColCount: Integer read FColCount;
    property RowCount: Integer read FRowCount;
    property Matrix[const Col, Row: Integer]: Byte read GetMatrix; default;
  end;

implementation

{ TMatrix }

procedure TMatrix.CalcColPairScores;
var
  Col1, Col2: Integer;
  Row: Integer;
begin
  for Col1 := 0 to ColCount - 1 do
  begin
    FColPairScores[Col1, Col1] := RowCount;
    for Col2 := Col1 + 1 to ColCount - 1 do
    begin
      for Row := 0 to RowCount - 1 do
        Inc(FColPairScores[Col1, Col2], (Matrix[Col1, Row] + Matrix[Col2, Row]) div 2);
      FColPairScores[Col2, Col1] := FColPairScores[Col1, Col2];
    end;
  end;
end;

procedure TMatrix.CalcRowPairScores;
var
  Col: Integer;
  Row1, Row2: Integer;
begin
  for Row1 := 0 to RowCount - 1 do
  begin
    FRowPairScores[Row1, Row1] := ColCount;
    for Row2 := Row1 + 1 to RowCount - 1 do
    begin
      for Col := 0 to ColCount - 1 do
        Inc(FRowPairScores[Row1, Row2], (Matrix[Col, Row1] + Matrix[Col, Row2]) div 2);
      FRowPairScores[Row2, Row1] := FRowPairScores[Row1, Row2];
    end;
  end;
end;

constructor TMatrix.Create(const Matrix: TByteMatrix);
begin
  FMatrix := Matrix;
  FColCount := Length(FMatrix);
  FRowCount := Length(FMatrix[0]);
  SetLength(FColPairScores, FColCount, FColCount);
  SetLength(FRowPairScores, FRowCount, FRowCount);
  CalcColPairScores;
  CalcRowPairScores;
end;

function TMatrix.GetColPairScore(const Col1, Col2: Integer): TScore;
begin
  Result := FColPairScores[Col1, Col2];
end;

function TMatrix.GetMatrix(const Col, Row: Integer): Byte;
begin
  Result := FMatrix[Col, Row];
end;

function TMatrix.GetRowPairScore(const Row1, Row2: Integer): TScore;
begin
  Result := FRowPairScores[Row1, Row2];
end;

end.
